//
//  PushViewController.swift
//  SmartStyle
//
//  Created by Utku Dora on 11/11/15.
//  Copyright © 2015 UDO. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Bolts
import Parse
import CoreLocation

class PushViewController: UIViewController ,CLLocationManagerDelegate{

    @IBAction func SendNotificationToAll(sender: AnyObject) {
        
        let push = PFPush()
        
    
        
        let locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()

        MyLocation.latitute = locationManager.location!.coordinate.latitude
        MyLocation.longitude = locationManager.location!.coordinate.longitude
//        let lat = locationManager.location!.coordinate.latitude
//        let lon = locationManager.location!.coordinate.longitude
//        
        
        
        
        
        
        
        
        
//        let myGeoPoint = PFGeoPoint(latitude: lat, longitude:lon)
//        let myParseId = PFUser.currentUser()?.objectId //PFUser.currentUser().objectId
//
//        
//        func sendPosition(userOfPosition: User) {
//            
//            let takePosition = PFObject(className: "Position")
//            
//            takePosition.setObject(myParseId!, forKey: "who") //who
//            takePosition.setObject(myGeoPoint, forKey: "where")
//            takePosition.saveInBackgroundWithBlock(nil)
//            
//        }
//        
//        
//       sendPosition(currentUser()!)
        
        
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            print("inside")
            if error == nil {
                print("working")
                // do something with the new geoPoint
                print(geoPoint)
              //  let location = geoPoint as String
                //push.setMessage("Alice: I am not feeling secure please come help!")
                //push.setMessage(location)
            }
        }
        
        
        
        
        push.setMessage("Alice: I am not feeling secure please come help!")
        push.sendPushInBackground()
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
            // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
